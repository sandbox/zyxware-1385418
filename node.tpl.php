<?php
?>
<div id="node-<?php print $node->nid; ?>" class="post node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<div class="post-heading"><<?php print $node_title_tag; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></<?php print $node_title_tag; ?>></div>

  <?php if ($submitted): ?>
    <div class="post-by clearfix">
      <ul>
          <li><?php print $submitted; ?></li>
           <?php if ($primary_category): ?>
             <li class="category"><?php print $primary_category; ?></li>
           <?php endif; ?>
      </ul>
      <a href="<?php print $node_url . '#comments'; ?>" class="comment_count" title="See comments"><?php print $comment_count; ?></a>
    </div>    
  <?php endif; ?>

  <div class="content clear-block post-content clearfix">
    <?php print $content ?>
  </div>
  <div class="post-bottom clearfix">
    <?php if ($taxonomy): ?>
      <div class="post-tags">Tags: <?php print $terms ?></div>
    <?php endif; ?>
    <a href="<?php print $node_url; ?>" class="read-more"><span>Continue reading</span></a>
    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>
</div>

