<?php
/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function phptemplate_settings($saved_settings) {
  $defaults = array(
    'facebook_page' => '',
    'twitter_username' => '',
    'linkedin_page' => '',
    'google_page' => '',
    'feed_url' => '',
  );
  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);
  $form['facebook_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Page URL'),
    '#description' => t('Facebook page URL to be linked from fixed social div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['facebook_page'],
  );
  $form['twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter URL'),
    '#description' => t('Twitter url to be linked from fixed social div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['twitter_url'],
  );
  $form['linkedin_page'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkedIn Page URL'),
    '#description' => t('LinkedIn page URL to be linked from fixed div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['linkedin_page'],
  );
  $form['google_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+ Page URL'),
    '#description' => t('Google+ page URL to be linked from fixed div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['google_page'],
  );
  $form['feed_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Feed URL'),
    '#description' => t('Feed URL to be linked from fixed div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['feed_url'],
  );
  $form['get_a_quote_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Get A Quote URL'),
    '#description' => t('Get A Quote URL to be linked from fixed div and social links in the footer. Leave empty to hide.'),
    '#default_value' => $settings['get_a_quote_url'],
  );
  return $form;
}

