<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="wrapper">
      <?php print $social_panel; ?>
      <div id="header" class="clearfix">
        <div class="wrap clearfix">
          <div class="top-left">
            <a href="<?php print check_url($home_url) ?>" id="logo" title="<?php print $site_name ?>"><img src="<?php print check_url($logo);?>" alt="<?php print check_url($site_title);?>" /></a>
            <span class="slogan"><?php if (isset($site_slogan)) { print ($site_slogan); } ?></span>
          </div>
          <div class="top-right">
            <?php if (isset($secondary_links)) : ?>
              <?php print theme('links', $secondary_links, array('class' => 'top-menu links secondary-links')) ?>
            <?php endif; ?>
            <?php print $search_box ?>
          </div><!--/ .top-right -->
        </div><!--/ .wrap -->
        <?php if (isset($primary_links)) : ?>
          <div class="wrap clearfix">
            <div id="menu">
            <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
            </div><!--/ #menu -->
          </div>
        <?php endif; ?>
      </div><!--/ #header -->
      <?php if ($show_inner_heading) : ?>
        <div id="inner-heading">
          <div class="wrap">
            <<?php print $page_title_tag; ?> id="page-title"><?php print $title; ?></<?php print $page_title_tag; ?>>
            <?php print $breadcrumb; ?>
          </div>
        </div><!--/#inner-heading -->
      <?php endif; ?>
      <div id="main" class="<?php print $main_div_class; ?>">
        <div class="wrap clearfix">
          <div id="content" class="clearfix">
            <?php if ($mission): print '<div id="mission"><span>&quot;</span>'. $mission .'<span>&quot;</span></div>'; endif; ?>
            <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
            <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
            <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php print $help; ?>
            <?php print $above_content; ?>
            <?php print $content; ?>
            <?php print $below_content; ?>
          </div><!--/ #content -->
          <div id="sidebar">
            <?php print $right; ?>
          </div><!--/#sidebar -->
        </div>
      </div><!--/ #main -->
      <div id="footer" class="clearfix">
        <div class="top">
          <div class="wrap clearfix">
            <div id="footer_1">
              <?php print $footer_1; ?>
            </div><!--/ .footer_1 -->
            <div id="footer_2">
              <?php print $footer_2; ?>
            </div><!--/ .footer_2 -->
            <div id="footer_3">
              <?php if ($footer_message): print '<div id="footer-message">' . $footer_message . '</div>'; endif; ?>
            </div><!--/ .sponsor -->
            <div id="quick-contact">
              <?php print $quick_contact; ?>
            </div>
          </div>
        </div>
      </div><!--/ #footer -->
      <div id="copyright">
        <div class="wrap clearfix">
          <div class="col1">Copyright &copy; <?php print(Date("Y")); ?> <?php print $site_name; ?> <span id="powered-by">Powered by <a href="http://www.drupal.org">Drupal</a> | <a href="http://www.zyxware.com">Theme by Zyxware</a></span></div>
          <div class="col2">
            <?php print $social_footer; ?>
          </div>
        </div><!--/ .wrap -->
      </div><!--/ #copyright -->
    </div><!--/ #wrapper -->
    <?php print $closure ?>
  </body>
</html>
