<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="wrapper">
      <div id="header" class="clearfix">
        <div class="wrap clearfix">
          <div class="top-left">
            <a href="#" id="logo" title="<?php print $site_name ?>"><img src="<?php print check_url($logo);?>" alt="<?php print check_url($site_title);?>" /></a>
            <span class="slogan"><?php if (isset($site_slogan)) { print ($site_slogan); } ?></span>
          </div>
          <div class="top-right">
          </div><!--/ .top-right -->
        </div><!--/ .wrap -->
        <div class="wrap clearfix">
          <div id="menu">
          </div><!--/ #menu -->
        </div>
      </div><!--/ #header -->
      <div id="inner-heading">
        <div class="wrap">
          <h1 id="page-title">Site under maintenance</h1>
        </div>
      </div><!--/#inner-heading -->
      <div id="main" class="full-page">
        <div class="wrap clearfix">
          <div id="content" class="clearfix" style="min-height: 200px;">
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php print $help; ?>
            <?php print $content; ?>
          </div><!--/ #content -->
        </div>
      </div><!--/ #main -->
      <div id="footer" class="clearfix">
        <div class="top">
          <div class="wrap clearfix">
          </div>
        </div>
      </div><!--/ #footer -->
      <div id="copyright">
        <div class="wrap clearfix">
          <div class="col1">Copyright &copy; <?php print(Date("Y")); ?> <?php print $site_name; ?> <span id="powered-by">Powered by <a href="http://www.drupal.org">Drupal</a> | <a href="http://www.zyxware.com">Theme by Zyxware</a></span></div>
          <div class="col2">
            <?php print $social_footer; ?>
          </div>
        </div><!--/ .wrap -->
      </div><!--/ #copyright -->
    </div><!--/ #wrapper -->
    <?php print $closure ?>
  </body>
</html>
