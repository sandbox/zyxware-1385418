$(document).ready(function() {
  $("#social-box .tab_content").hide(); //Hide all content
  $("#social-box ul.tabs li:first").addClass("active").show(); //Activate first tab
  $("#social-box .tab_content:first").show(); //Show first tab content
  $("#social-box ul.tabs li").click(function() {
    $("#social-box ul.tabs li").removeClass("active"); //Remove any "active" class
    $(this).addClass("active"); //Add "active" class to selected tab
    $("#social-box .tab_content").hide(); //Hide all tab content
    var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
    $(activeTab).fadeIn(); //Fade in the active content
    return false;
  });
  height = $("#footer_1").height() ;
  if ($("#footer_2").height() > height) height = $("#footer_2").height();
  if ($("#footer_3").height() > height) height = $("#footer_2").height();
  $("#footer_1").height(height);
  $("#footer_2").height(height);
  $("#footer_3").height(height);
  $('#quick-contact form').submit(function(){
    $('#quick-contact input[name="subject"]').val($('#quick-contact textarea[name="message"]').val());
  });
});
