<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="clear-block module-box <?php print $block_class ?> block block-<?php print $block->module ?>">

<?php if (!empty($block->subject)): ?>
  <div class="heading"><div class="block-title"><?php print $block->subject ?></div></div>
<?php endif;?>

  <div class="content"><?php print $block->content ?></div>
</div>
