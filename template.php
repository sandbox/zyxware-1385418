<?php

/**
* Implementation of hook_theme().
*/
function zyxware_theme(){
  return array(
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'social_panel' => array(
      'arguments' => array(),
    ), 
    'social_footer' => array(
      'arguments' => array(),
    ), 
    'quick_contact' => array(
      'arguments' => array(),
    ), 
  );
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function zyxware_breadcrumb($breadcrumb) {
  $breadcrumb_items = ''; 
  if (!empty($breadcrumb)) {
    $breadcrumb_items = '<li class="first">'. implode('</li></li>', $breadcrumb) .'</li>';
  }
  return '<ul class="breadcrumb">'. $breadcrumb_items . '<li><span>'. drupal_get_title() .'</span></li></ul>';
}

/**
 * Implementation of hook_preprocess_page().
 */
function zyxware_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  if ($vars['main_div_class'] == "") {
    $vars['main_div_class'] = 'inner-page';
  }
  
  global $user; 

  if ($user->uid != 0) {
    $new_links['account-link'] = array('attributes' => array('title' => 'Account link'), 'href' => 'user', 'title' => 'My Account');
    $new_links['logout-link'] = array('attributes' => array('title' => 'Logout link'), 'href' => 'logout', 'title' => 'Logout');
  } else {
    $new_links['login-link'] = array('attributes' => array('title' => 'Login link'), 'href' => 'user', 'title' => 'Login');
  }
  $vars['secondary_links'] = array_merge_recursive($vars['secondary_links'], $new_links);

  $vars['social_panel'] = '';
  if (arg(0) != 'admin') {
    $vars['social_panel'] = theme('social_panel');
  }
  $vars['social_footer'] = theme('social_footer');
  // Create default quick contact form from site contact form.
  // Ideally this should be custom handled to prevent spam.
  if (!isset($vars['quick_contact'])) {
    $vars['quick_contact'] = theme('quick_contact');
  }  
  
  // For node pages don't show the page title in the header as h1. Use div instead
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $vars['page_title_tag'] = 'div';
    $vars['title'] = 'Articles';
  }
  else {
    $vars['page_title_tag'] = 'h1';
    if ($vars['title'] == '' ) {
      $vars['title'] = 'Latest Articles';
    }
  }
  // If custom home page is not handled via module then set site home as the
  // home page and assume that home is a listing page and show inner heading
  if ($vars['home_url'] == '' ) {
    $vars['home_url'] = $vars['front_page'];
    if (!isset($vars['show_inner_heading'])) {
      $vars['show_inner_heading'] = TRUE;  
    }  
  }
}

/**
 * Implementation of hook_preprocess_block().
 */
function zyxware_preprocess_block(&$vars) {
  static $i = 0;
  if ($vars['block']->region == 'right') {
    $block_colors = array('blue', 'red', 'green', 'orange');
    $vars['block_class'] = $block_colors[$i] . 'box';
    $i++; 
    if ($i == count($block_colors)) { 
      $i = 0;
    }
  }
}

/**
 * Implementation of hook_preprocess_node().
 */
function zyxware_preprocess_node(&$vars) {
  $node = $vars['node'];
  if ($vars['page']) {
    $vars['node_title_tag'] = 'h1';
  }
  else {
    $vars['node_title_tag'] = 'h2';
  }
  // Set primary_category as the first term if it has not been set yet
  if (!isset($vars['primary_category'])) {
    $terms = taxonomy_node_get_terms($node);
    if (count($terms)) {
      $term = array_shift($terms);
      $vars['primary_category'] = l($term->name, 'taxonomy/term/' . $term->tid);
    }
  }
}

/**
 * Add a "Comments" heading above comments except on forum pages.
 */
function zyxware_preprocess_comment_wrapper(&$vars) {
  if ($vars['content'] && $vars['node']->type != 'forum') {
    $vars['content'] = '<h2 class="comments">'. t('Comments') .'</h2>'.  $vars['content'];
  }
}

/**
 * Theme function for comment_form.
 */
function zyxware_comment_form($form) {
  global $user;
 
  if ($user->uid != 0) {
    $form['_author']['#title'] = t('Name');
  } else {
    $form['name']['#title'] = t('Name');
    // Unset format information for anonymous users
    $form['comment_filter']['format'] = NULL;
    $form['mail']['#description'] = t('Your email address will not be published and will be kept confidential.');
  }
  $form['homepage']['#title'] = t('Website');
  $form['subject']['#size'] = 40;

  $form['comment_filter']['comment']['#title']  = t('Your message');
  $form['comment_filter']['comment']['#rows']  = 8;
  $form['comment_filter']['comment']['#cols']  = 55;
 
  // Remove the preview button
   $form['preview'] = NULL;
 
  return drupal_render($form);
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function zyxware_menu_local_tasks() {
  return menu_primary_local_tasks();
}

/**
 * Returns the themed submitted-by string for the comment.
 *
 * @ingroup themeable
 */
function zyxware_comment_submitted($comment) {
  return t('!username <span class="date">wrote on !datetime</span>',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

/**
 * Returns the themed submitted-by string for the node.
 *
 * @ingroup themeable
 */
function zyxware_node_submitted($node) {
  return t('By !username on <span class="date">!datetime</span>',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Generate the HTML output for a menu tree
 *
 * @ingroup themeable
 */
function zyxware_menu_tree($tree) {
  return '<ul class="menu listing">'. $tree .'</ul>';
}

/**
 * Generate the HTML output for the fixed social div
 *
 * @ingroup themeable
 */
function zyxware_social_panel() {
  $links = '';
  if ($value = theme_get_setting('get_a_quote_url')) {
    $links .= '<li class="get-a-quote"><a href="#">Get a Quote</a></li>';
  }  
  if ($value = theme_get_setting('facebook_page')) {
    $links .= '<li class="facebook"><a href="' . $value . '" title="Facebook">Facebook</a></li>'; 
  }  
  if ($value = theme_get_setting('twitter_url')) {
    $links .= '<li class="twitter"><a href="' . $value . '" title="Twitter">Twitter</a></li>'; 
  }  
  if ($value = theme_get_setting('google_page')) {
    $links .= '<li class="google"><a href="' . $value . '" title="Google+">Google+</a></li>'; 
  }  
  if ($value = theme_get_setting('feed_url')) {
    $links .= '<li class="feed"><a href="' . $value . '" title="Feed">Feed</a></li>'; 
  }
  $html = '';
  if ($links != '') {
    $html = '<ul id="social-panel">' . $links . '</ul>';
  }
  return $html;
}

/**
 * Generate the HTML output for the social links in the footer
 *
 * @ingroup themeable
 */
function zyxware_social_footer() {
  $html = '';
  if ($value = theme_get_setting('get_a_quote_url')) {
    $html .= '<a href="' . $value . '" class="get-a-quote">Get a Quote</a>';
  }  
  $links = '';
  if ($value = theme_get_setting('facebook_page')) {
    $links .= '<li class="facebook"><a href="' . $value . '" title="Facebook">Facebook</a></li>'; 
  }  
  if ($value = theme_get_setting('twitter_url')) {
    $links .= '<li class="twitter"><a href="' . $value . '" title="Twitter">Twitter</a></li>'; 
  }  
  if ($value = theme_get_setting('linkedin_page')) {
    $links .= '<li class="linkedin"><a href="' . $value . '" title="Linkedin">Linkedin</a></li>'; 
  }  
  if ($value = theme_get_setting('google_page')) {
    $links .= '<li class="google"><a href="' . $value . '" title="Google+">Google+</a></li>'; 
  }  
  if ($value = theme_get_setting('feed_url')) {
    $links .= '<li class="feed"><a href="' . $value . '" title="Feed">Feed</a></li>'; 
  }

  if ($links != '') {
    $html .= '<ul id="social-footer">' . $links . '</ul>';
  }
  return $html;
}

/**
 * Generate the HTML output for the fixed social div
 *
 * @ingroup themeable
 */
function zyxware_quick_contact() {
  $html = '';
  if (module_exists('contact')) {
    module_load_include('inc', 'contact', 'contact.pages');
    $form = drupal_get_form('contact_mail_page');
    $html = '<div class="block-title">Quick Contact</div>' . $form;
    /*    
    <input type="text" class="textbox" value="Name" onblur="if(this.value=='') this.value='Name';" onfocus="if(this.value=='Name') this.value='';" />
    <input type="text" class="textbox" value="Email" value="Name" onblur="if(this.value=='') this.value='Email';" onfocus="if(this.value=='Email') this.value='';" />
    <div class="textarea-bg"><textarea name="textarea" cols="0" rows="0"  onblur="if(this.value=='') this.value='Message';" onfocus="if(this.value=='Message') this.value='';">Message</textarea></div>
    <input type="button" class="submit" value="Submit" />
    */
  }
  return $html;
}
